<?php
   /*
   Plugin Name: NBA Franchise Post Type   
   Description: A plugin to create a new post type specifically for adding new franchises
   Version: 1.0
   Author: Neal   
   */

/* 
* I leaned on this a fair bit: https://www.smashingmagazine.com/2012/11/complete-guide-custom-post-types/
*/
function nba_franchise_post_type() {
  $labels = array(
    'name'               => _x( 'Franchises', 'post type general name' ),
    'singular_name'      => _x( 'Franchise', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'Franchise' ),
    'add_new_item'       => __( 'Add New Franchise' ),
    'edit_item'          => __( 'Edit Franchise' ),
    'new_item'           => __( 'New Franchise' ),
    'all_items'          => __( 'All Franchises' ),
    'view_item'          => __( 'View Franchises' ),
    'search_items'       => __( 'Search Franchises' ),
    'not_found'          => __( 'No items found' ),
    'not_found_in_trash' => __( 'No items found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Franchises'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our Franchises and Franchise related info',
    'public'        => true,
    'menu_position' => 3,
    'supports'      => array( 'title', 'thumbnail', 'custom-fields' ),
    'has_archive'   => true,    
    'show_in_rest' =>  true
  );
  register_post_type( 'franchises', $args ); 
}
add_action( 'init', 'nba_franchise_post_type' );

/* Create the meta boxes */
function nba_add_franchise_meta_boxes() {
  
  add_meta_box( 
    'conference', // Unique ID.
    esc_html__( 'Conference', 'myplugin_textdomain' ), // Title off the field that is displayed
    'franchise_conference_dropdown', // Call this function to display the input box int the post editor
    'franchises', //The post type to add this to (i.e. franchise custom post type as registerd above in nba_franchise_post_type())
    'normal', // Location on page
    'high'
);

	add_meta_box( 
        'founded', // Unique ID.
        esc_html__( 'Year founded', 'myplugin_textdomain' ), // Title off the field that is displayed
        'founded_content', // Call this function to display the input box int the post editor
        'franchises', //The post type to add this to (i.e. franchise custom post type as registerd above in nba_franchise_post_type())
        'normal', // Location on page
        'high'
    );

    add_meta_box( 
      'arena', // Unique ID.
      esc_html__( 'Arena', 'myplugin_textdomain' ), // Title off the field that is displayed
      'arena_content', // Call this function to display the input box int the post editor
      'franchises', //The post type to add this to (i.e. franchise custom post type as registerd above in nba_franchise_post_type())
      'normal', // Location on page
      'high'
    );

    add_meta_box( 
      'owner', // Unique ID.
      esc_html__( 'Owner', 'myplugin_textdomain' ), // Title off the field that is displayed
      'owner_content', // Call this function to display the input box int the post editor
      'franchises', //The post type to add this to (i.e. franchise custom post type as registerd above in nba_franchise_post_type())
      'normal', // Location on page
      'high'
    );
    
    add_meta_box( 
      'head_coach', // Unique ID.
      esc_html__( 'Head Coach', 'myplugin_textdomain' ), // Title off the field that is displayed
      'head_coach_content', // Call this function to display the input box int the post editor
      'franchises', //The post type to add this to (i.e. franchise custom post type as registerd above in nba_franchise_post_type())
      'normal', // Location on page
      'high'
    );

    add_meta_box( 
      'g_league', // Unique ID.
      esc_html__( 'G League Affiliate', 'myplugin_textdomain' ), // Title off the field that is displayed
      'g_league_content', // Call this function to display the input box int the post editor
      'franchises', //The post type to add this to (i.e. franchise custom post type as registerd above in nba_franchise_post_type())
      'normal', // Location on page
      'high'
    );

    add_meta_box( 
      'franchise_website', // Unique ID.
      esc_html__( 'Website', 'myplugin_textdomain' ), // Title off the field that is displayed
      'franchise_website_content', // Call this function to display the input box int the post editor
      'franchises', //The post type to add this to (i.e. franchise custom post type as registerd above in nba_franchise_post_type())
      'normal', // Location on page
      'high'
    );	
}
add_action( 'add_meta_boxes', 'nba_add_franchise_meta_boxes' );


/* HTML for the actual input field of the meta boxes */
function franchise_conference_dropdown($post) {
        
  $value = get_post_meta($post->ID, 'conference', true); 
  
  ?>
  <label for="conference">Choose a conference</label><br>
  
  <select name="conference" id="conference" class="postbox">
      <option value="Eastern" <?php selected($value, 'Eastern'); ?>>Eastern</option>
      <option value="Western" <?php selected($value, 'Western'); ?>>Western</option>      
  </select>
  <?php
}

function founded_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'founded_nonce' );?>
	  
	<label for="founded"></label>
	<input type="text" id="founded" name="founded" size="4" maxlength="4" value="<?php echo esc_attr( get_post_meta( $post->ID, 'founded', true ) ); ?>" />
	  
	<?php
}

function arena_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'arena_nonce' );?>
	  
	<label for="arena"></label>
	<input type="text" id="arena" name="arena" size="30" value="<?php echo esc_attr( get_post_meta( $post->ID, 'arena', true ) ); ?>" />
	  
	<?php
}

function owner_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'owner_nonce' );?>
	  
	<label for="owner"></label>
	<input type="text" id="owner" name="owner" size="20" value="<?php echo esc_attr( get_post_meta( $post->ID, 'owner', true ) ); ?>" />
	  
	<?php
}

function head_coach_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'head_coach_nonce' );?>
	  
	<label for="head_coach"></label>
	<input type="text" id="head_coach" name="head_coach" size="20" value="<?php echo esc_attr( get_post_meta( $post->ID, 'head_coach', true ) ); ?>" />
	  
	<?php
}

function g_league_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'g_league_nonce' );?>
	  
	<label for="g_league"></label>
	<input type="text" id="g_league" name="g_league" size="20" value="<?php echo esc_attr( get_post_meta( $post->ID, 'g_league', true ) ); ?>" />
	  
	<?php
}

function franchise_website_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'franchise_website_nonce' );?>
	  
	<label for="franchise_website"></label>
	<input type="text" id="franchise_website" name="franchise_website" size="20" value="<?php echo esc_attr( get_post_meta( $post->ID, 'franchise_website', true ) ); ?>" />
	  
	<?php
}


/* 
* Gotten from here: https://codex.wordpress.org/Plugin_API/Action_Reference/save_post 
*/

/**
 * Save post metadata when a post is saved.
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function save_franchise_meta( $post_id, $post, $update ) {

    /*
     * In production code, $slug should be set only once in the plugin,
     * preferably as a class property, rather than in each function that needs it.
     */
    $post_type = get_post_type($post_id);

    // - Update the post's metadata.   

    if ( array_key_exists('conference', $_POST)) {
      update_post_meta( $post_id, 'conference', $_POST['conference'] );
    }

    if ( array_key_exists('founded', $_POST)) {
      update_post_meta( $post_id, 'founded', $_POST['founded'] );
    }

    if ( isset( $_POST['arena'] ) ) {
      update_post_meta( $post_id, 'arena', sanitize_text_field( $_POST['arena'] ) );
    }

    if ( isset( $_POST['owner'] ) ) {
      update_post_meta( $post_id, 'owner', sanitize_text_field( $_POST['owner'] ) );
    }

    if ( isset( $_POST['head_coach'] ) ) {
      update_post_meta( $post_id, 'head_coach', sanitize_text_field( $_POST['head_coach'] ) );
    }

    if ( isset( $_POST['g_league'] ) ) {
      update_post_meta( $post_id, 'g_league', sanitize_text_field( $_POST['g_league'] ) );
    }

    if ( isset( $_POST['franchise_website'] ) ) {
      update_post_meta( $post_id, 'franchise_website', sanitize_text_field( $_POST['franchise_website'] ) );
    }
}
/* save_post_{post_type} was introduced in WP 3.7 it seems, so we can use that (as I am below with my 'franchises' post type) instead of the generic save_post */ 
add_action( 'save_post_franchises', 'save_franchise_meta', 10, 3 );



$fields = ['conference', 'founded', 'arena', 'owner', 'head_coach', 'g_league', 'franchise_website'];

foreach ( $fields as $field ) {
  $field_args = array( // Validate and sanitize the meta value.
      // Note: currently (4.7) one of 'string', 'boolean', 'integer',
      // 'number' must be used as 'type'. The default is 'string'.
      'type'         => 'string',
      // Shown in the schema for the meta key.
      'description'  => 'A meta key associated with a string meta value.',
      // Return a single value of the type.
      'single'       => true,
      // Show in the WP REST API response. Default: false.
      'show_in_rest' => true,
  );
  register_meta( 'post', $field, $field_args );
}
?>