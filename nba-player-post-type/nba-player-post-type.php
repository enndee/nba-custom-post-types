<?php
   /*
   Plugin Name: NBA Players Post Type   
   Description: A plugin to create a new post type specifically for adding new players
   Version: 1.0
   Author: Neal   
   */

/* 
* I leaned on this a fair bit: https://www.smashingmagazine.com/2012/11/complete-guide-custom-post-types/
*/
function nba_player_post_type() {
  $labels = array(
    'name'               => _x( 'Players', 'post type general name' ),
    'singular_name'      => _x( 'Player', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'player' ),
    'add_new_item'       => __( 'Add New Player' ),
    'edit_item'          => __( 'Edit Player' ),
    'new_item'           => __( 'New Player' ),
    'all_items'          => __( 'All Players' ),
    'view_item'          => __( 'View Players' ),
    'search_items'       => __( 'Search Players' ),
    'not_found'          => __( 'No items found' ),
    'not_found_in_trash' => __( 'No items found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Players'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our Players and Player related info',
    'public'        => true,
    'menu_position' => 3,
    'supports'      => array( 'title', 'thumbnail', 'custom-fields' ),
    'has_archive'   => true,    
    'show_in_rest' =>  true
  );
  register_post_type( 'players', $args ); 
}
add_action( 'init', 'nba_player_post_type' );

/* Create the meta boxes */
function nba_add_player_meta_boxes() {
    
	add_meta_box( 
        'team', // Unique ID.
        esc_html__( 'Team', 'myplugin_textdomain' ), // Title of the field that is displayed
        'player_team_dropdown', // Call this function to display the input box int the post editor
        'players', //The post type to add this to (i.e. NBA Player custom post type as registerd above in nba_player_post_type())
        'normal', // Location on page
        'high'
    );
	
	add_meta_box( 
        'number', // Unique ID.
        esc_html__( 'Squad number', 'myplugin_textdomain' ), // Title off the field that is displayed
        'number_content', // Call this function to display the input box int the post editor
        'players', //The post type to add this to (i.e. Player custom post type as registerd above in nba_player_post_type())
        'normal', // Location on page
        'high'
    );
    
    add_meta_box( 
        'position', // Unique ID.
        esc_html__( 'Position', 'myplugin_textdomain' ), // Title off the field that is displayed
        'player_position_dropdown', // Call this function to display the input box int the post editor
        'players', //The post type to add this to (i.e. Player custom post type as registerd above in nba_player_post_type())
        'normal', // Location on page
        'high'
  );
  
  add_meta_box( 
    'height', // Unique ID.
    esc_html__( 'Height', 'myplugin_textdomain' ), // Title off the field that is displayed
    'height_content', // Call this function to display the input box int the post editor
    'players', //The post type to add this to (i.e. Player custom post type as registerd above in nba_player_post_type())
    'normal', // Location on page
    'high'
  );

  add_meta_box( 
    'dob', // Unique ID.
    esc_html__( 'Date of Birth', 'myplugin_textdomain' ), // Title off the field that is displayed
    'dob_content', // Call this function to display the input box int the post editor
    'players', //The post type to add this to (i.e. Player custom post type as registerd above in nba_player_post_type())
    'normal', // Location on page
    'high'
  );
	
}
add_action( 'add_meta_boxes', 'nba_add_player_meta_boxes' );

function player_team_dropdown($post) {
        
  $teams = ['Atlanta Hawks', 'Boston Celtics', 'Brooklyn Nets', 'Charlotte Hornets', 'Chicago Bulls', 'Cleveland Cavaliers', 'Dallas Mavericks', 'Denver Nuggets', 'Detroit Pistons', 'Golden State Warriors', 'Houston Rockets', 'Indiana Pacers', 'LA Clippers', 'LA Lakers', 'Memphis Grizzlies', 'Miami Heat', 'Milwaukee Bucks', 'Minnesota Timberwolves', 'New Orleans Pelicans', 'New York Knicks', 'Oklahoma City Thunder', 'Orlando Magic', 'Philadelphia 76ers', 'Phoenix Suns', 'Portland Trail Blazers', 'Sacramento Kings', 'San Antonio Spurs', 'Toronto Raptors', 'Utah Jazz', 'Washington Wizards'];
  
  $value = get_post_meta($post->ID, 'team', true); 
  
  ?>
  <label for="team">Choose a team</label><br>
  
  <select name="team" id="team" class="postbox">
    <?php foreach ( $teams as $team ) { ?>
      <option value="<?php echo $team; ?>" <?php selected($value, $team); ?>><?php echo $team; ?></option>
      <?php } ?>
  </select>
  <?php
}

function player_position_dropdown($post) {
        
  $value = get_post_meta($post->ID, 'position', true); 
  
  ?>
  <label for="position">Choose a position</label><br>
  
  <select name="position" id="position" class="postbox">
      <option value="G" <?php selected($value, 'G'); ?>>G</option>
      <option value="F" <?php selected($value, 'F'); ?>>F</option>      
      <option value="C" <?php selected($value, 'C'); ?>>C</option>
  </select>
  <?php
}


/* HTML for the actual input field of the meta boxes */
function number_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'number_nonce' );?>
	  
	<label for="number"></label>
	<input type="text" id="number" name="number" size="2" maxlength="2" value="<?php echo esc_attr( get_post_meta( $post->ID, 'number', true ) ); ?>" />
	  
	<?php
}

function height_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'height_nonce' );?>
	  
	<label for="height"></label>
	<input type="text" id="height" name="height" size="4" maxlength="4" value="<?php echo esc_attr( get_post_meta( $post->ID, 'height', true ) ); ?>" />
	  
	<?php
}

function dob_content( $post ) {
	
	wp_nonce_field( plugin_basename( __FILE__ ), 'dob_nonce' );?>
	  
	<label for="dob"></label>
	<input type="text" id="dob" name="dob" size="10" maxlength="10" value="<?php echo esc_attr( get_post_meta( $post->ID, 'dob', true ) ); ?>" />
	  
	<?php
}


/* 
* Gotten from here: https://codex.wordpress.org/Plugin_API/Action_Reference/save_post 
*/

/**
 * Save post metadata when a post is saved.
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function save_player_meta( $post_id, $post, $update ) {

    /*
     * In production code, $slug should be set only once in the plugin,
     * preferably as a class property, rather than in each function that needs it.
     */
    $post_type = get_post_type($post_id);

    // - Update the post's metadata.

    if ( array_key_exists('team', $_POST)) {
      update_post_meta( $post_id, 'team', $_POST['team'] );
    }

    if ( isset( $_POST['number'] ) ) {
      update_post_meta( $post_id, 'number', sanitize_text_field( $_POST['number'] ) );
    }

    if ( array_key_exists('position', $_POST)) {
      update_post_meta( $post_id, 'position', $_POST['position'] );
    }

    if ( isset( $_POST['height'] ) ) {
      update_post_meta( $post_id, 'height', sanitize_text_field( $_POST['height'] ) );
    }

    if ( isset( $_POST['dob'] ) ) {
      update_post_meta( $post_id, 'dob', sanitize_text_field( $_POST['dob'] ) );
    }
}
/* save_post_{post_type} was introduced in WP 3.7 it seems, so we can use that (as I am below with my 'Players' post type) instead of the generic save_post */ 
add_action( 'save_post_players', 'save_player_meta', 10, 3 );



$fields = ['team', 'number', 'position', 'height', 'dob'];

foreach ( $fields as $field ) {
  $field_args = array( // Validate and sanitize the meta value.
      // Note: currently (4.7) one of 'string', 'boolean', 'integer',
      // 'number' must be used as 'type'. The default is 'string'.
      'type'         => 'string',
      // Shown in the schema for the meta key.
      'description'  => 'A meta key associated with a string meta value.',
      // Return a single value of the type.
      'single'       => true,
      // Show in the WP REST API response. Default: false.
      'show_in_rest' => true,
  );
  register_meta( 'post', $field, $field_args );
}

?>